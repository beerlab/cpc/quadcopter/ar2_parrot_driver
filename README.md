### Usage

1. Clone `ar2_parrot_driver` repository
```
git clone https://gitlab.com/beerlab/cpc/quadcopter/ar2_parrot_driver.git
```

2. Go to the new directory and build the docker image `ros-m`
```
cd ./ar2_parrot_driver
bash build.bash
```

3. Run Container from new image `ros-m`, initialize catkin workspace and build all catkin packages
```
bash start.bash
cd /workspace/src/ && catkin_init_workspace
cd /workspace/ && catkin_make
```

4. Restart docker container
```
exit
bash start.bash
```

5. Turn on ardrone and connect to the wifi `ardrone_xxxxx`. To check the connection make ping from docker container
```
ping 192.168.1.1
```

6. Run tmux and ardrone driver
```
tmux
roslaunch ardrone_autonomy ardrone.launch
```
