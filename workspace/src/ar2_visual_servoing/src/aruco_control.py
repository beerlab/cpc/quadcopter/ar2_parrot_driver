#!/usr/bin/env python3

# ROS
import rospy
import rospkg
import json
from sensor_msgs.msg import Image, Imu
from geometry_msgs.msg import Twist
from std_msgs.msg import Empty as EmptyMsg
from std_srvs.srv import Empty
from tf.transformations import quaternion_matrix, euler_from_quaternion, euler_from_matrix

import cv_bridge # CvBridge, CvBridgeError

# Other
import cv2
import numpy as np
import sys
from enum import Enum


class CAMERA_CHANNEL(Enum):
    FRONT = 0
    BOTTOM = 1

class CAMERA_REF(Enum):
    PROJECTION_FRAME = 0
    ROBOT_FRAME = 1

''' 
The first element of tuple true if drone should land, else false
The secobd element - list of active directions with next order: x-0, y-1, z-2
So, for example, ARUCO_STABILIZATION_XYZ means that drone shouldn't land, and that all 3 axes are controlled
'''

class CONTROL_MODE(Enum):
    ARUCO_STABILIZATION_XY = (False, [0, 1])
    ARUCO_STABILIZATION_XYZ = (False, [0, 1, 2])
    ARUCO_STABILIZATION_LAND = (True, [0, 1, 2])

class PID:

    def __init__(self, kp, ki, kd, dimensions):

        self._dimensions = dimensions
        self._P = 0
        self._I = 0
        self._D = 0

        self._kp = kp
        self._ki = ki
        self._kd = kd

        self._current_time = rospy.Time.now().to_sec()
        self._last_time = rospy.Time.now().to_sec()
        self._e = 0
        self._control = np.zeros((self._dimensions,))


    def reset(self):
        self._P = 0
        self._I = 0
        self._D = 0

        self._e = 0
        self._control = np.zeros((self._dimensions,))

    def update_pid(self, e):

        self._last_time = self._current_time
        self._current_time = rospy.Time.now().to_sec()
        self._T = self._current_time - self._last_time

        if self._T > 0.1:
            self.reset()
            return
    
        self._P = self._kp @ e
        self._D = self._kd @ e/self._T
        self._I += self._ki @ (e + self._e) * self._T/2
        self._I = self.saturation(self._I, s=0.1)
        self._e = e

        self._control = self.saturation(self._P + self._I + self._D, s=0.2)

    def saturation(self, data, s):
        return np.clip(data, -s, s)

    def get_control(self):
        return self._control

def rot_x(q):
    return np.array([
        [1,         0,          0],
        [0, np.cos(q), -np.sin(q)],
        [0, np.sin(q),  np.cos(q)]
    ])

def rot_y(q):
    return np.array([
        [ np.cos(q), 0, np.sin(q)],
        [         0, 1,         0],
        [-np.sin(q), 0, np.cos(q)]
    ])

def rot_z(q):
    return np.array([
        [np.cos(q), -np.sin(q), 0],
        [np.sin(q),  np.cos(q), 0],
        [        0,          0, 1]
    ])

global_dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_250)
global_parameters =  cv2.aruco.DetectorParameters()
global_detector = cv2.aruco.ArucoDetector(global_dictionary, global_parameters)
global_cv_bridge = cv_bridge.CvBridge()

def get_frames(rgb_raw):
        cv_image = None
        try:
            cv_image = global_cv_bridge.imgmsg_to_cv2(rgb_raw, "passthrough")
        except cv_bridge.CvBridgeError as e:
            print(e)

        return cv_image

class DroneControl:
    
    def __init__(self, config, camera_channel):
        control_params = config['control_params']
        topics = config['topics']

        self._cv_debug = config['CV_DEBUG']
        self._marker_length = config['aruco_marker']['length']
        self._land_threshold = config['land_threshold']
        self._control_mode = CONTROL_MODE[config['control_mode']]
        self._camera_ref = CAMERA_REF[config['camera_ref']]
        self._principal_point = np.array(config['camera_ref'])

        self._pid = PID(control_params[config['control_mode']]['kp'], control_params[config['control_mode']]['ki'], control_params[config['control_mode']]['kd'], control_params[config['control_mode']]['dimensions'])

        self._orientation = [0, 0, 0, 1]
        self._position = [0, 0, 0]
        self._reset_counter = 0
        
        if camera_channel.value == 0:
            self._camera_params = topics['front_camera']
        else:
            self._camera_params = topics['bottom_camera']

        self._principal_point = np.array(self._camera_params['principal_point'])

        rospy.Subscriber(self._camera_params['topic'], Image, self.image_callback, queue_size=1)
        rospy.Subscriber(topics['imu']['topic'], Imu, self.imu_callback, queue_size=1)

        self._cmd_vel_pub = rospy.Publisher(topics['cmd_vel']['topic'], Twist, queue_size=1)
        self._land_pub = rospy.Publisher(topics['land']['topic'], EmptyMsg, queue_size=10)
    
    def start(self):
        ''' Start controll'''
        print("Started Drone Control")
        rospy.spin()

    def land(self):
        ''' Method send command for drone to land'''
        print("Landing")
        empty_msg = EmptyMsg()
        self._land_pub.publish(empty_msg)
        rospy.sleep(10.0)
    
    def update_orientation(self, orientation):
        ''' Method to udpate orientation'''
        self._orientation = orientation

    def update_position(self, position):
        ''' Method to udpate position'''
        self._position = position

    def find_aruco(self, frame):
        ''' Method to find aruco and draw it on frame'''
        markerCorners, markerIds, rejectedCandidates = global_detector.detectMarkers(frame)
        success = False
        refference_point = None

        if markerIds is not None:
            success = True
            aruco_idx = 0

            left_top_corner = tuple(map(int, markerCorners[aruco_idx][0][0]))
            right_bottom_corner = tuple(map(int, markerCorners[aruco_idx][0][2]))
            refference_point = tuple(map(int, (np.array(left_top_corner) + np.array(right_bottom_corner))/2))
            
            if self._cv_debug:
                cv2.aruco.drawDetectedMarkers(frame, markerCorners, markerIds)
                cv2.circle(frame, tuple(map(int, refference_point)), radius=10, color=(0, 0, 255), thickness=-1)
                cv2.line(frame, tuple(map(int, [self._principal_point[1], self._principal_point[0]])), tuple(map(int, refference_point)), (255, 0, 0), 2)
            
        return success, frame, refference_point, markerCorners

    def position_estimate(self, projection_point, markerCorners, aruco_idx):
        ''' Method to estimate current pose of drone, using information about aruco'''
        euler = euler_from_quaternion(self._orientation)
        top = np.array(markerCorners[aruco_idx][0][0]) - np.array(markerCorners[aruco_idx][0][1])
        projection_top = np.array([-top[1], -top[0]])

        theta = np.arctan2(projection_top[1], projection_top[0])

        top_length = np.linalg.norm(markerCorners[aruco_idx][0][0] - markerCorners[aruco_idx][0][1])
        copter_h = self._camera_params['fh']*self._marker_length / top_length
        camera_point = np.array([
            projection_point[0] * copter_h / self._camera_params['fh'], 
            projection_point[1] * copter_h / self._camera_params['fh'], 
            copter_h
        ])

        rot = rot_y(euler[1]) @ rot_x(euler[0])
        new_position = rot.T @ camera_point
        print('position', new_position)
        
        return new_position, theta

    def image_callback(self, msg):
        
        frame = get_frames(rgb_raw=msg)
        aruco_found, frame, refference_point, markerCorners = self.find_aruco(frame)
        
        if aruco_found:
            projection_point = np.array([-refference_point[1], -refference_point[0]]) + self._principal_point

            if self._camera_ref == CAMERA_REF.ROBOT_FRAME:
                new_position, theta = self.position_estimate(projection_point, markerCorners, 0)
                self.update_position(new_position)

            if self._camera_ref == CAMERA_REF.PROJECTION_FRAME:
                # Calculate error
                err = projection_point.copy()
                err = np.array([err[0]/msg.height, err[1]/msg.width, 0.0])
                theta = 0

            if self._camera_ref == CAMERA_REF.ROBOT_FRAME:
                # Calculate error
                err = self._position.copy()
                err[2] = self._land_threshold - self._position[2]

            # set to zero all axes which are not belongs to active directions
            other_indxes = np.setdiff1d(np.arange(self._pid._dimensions), np.array(self._control_mode.value[1]))
            err[other_indxes] = 0.0

            # land drone
            if self._control_mode.value[0]:
                if self._position[2] <= self._land_threshold:
                    self.land()
                    return
                
            self._pid.update_pid(err)
            control = self._pid.get_control()
            print('control', control)
            # print('err', err)

            move_cmd = Twist()
            move_cmd.linear.x = control[0]
            move_cmd.linear.y = control[1]
            move_cmd.linear.z = control[2]
            move_cmd.angular.z = 0.0 # 0.05*(-theta)

            self._cmd_vel_pub.publish(move_cmd)
        else:
            if self._reset_counter > 3:
                self._pid.reset()
                control = self._pid.get_control()
                move_cmd = Twist()
                move_cmd.linear.x = control[0]
                move_cmd.linear.y = control[1]
                move_cmd.linear.z = control[2]
                move_cmd.angular.z = 0.0
                self._cmd_vel_pub.publish(move_cmd)
                self._reset_counter = 0

            print('none')
            self._reset_counter += 1

        print('-----------')
        if self._cv_debug:
            cv2.imshow('frame', frame)
            cv2.waitKey(1)

    def imu_callback(self, msg):
        ''' Method get access to imu data'''
        quaternion = [msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w]
        self.update_orientation(quaternion)



if __name__ == '__main__':
    # Init ros
    rospy.init_node('aruco_control')
    rospack = rospkg.RosPack()

    #Get current package dir
    dir_path = rospack.get_path('ar2_visual_servoing')

    # Read config
    with open(dir_path + "/config/config.json", 'r') as config_file:
        config = json.load(config_file)
        control_params = config['control_params']
        topics = config['topics']

    camera_channel = CAMERA_CHANNEL.FRONT

    # Switch camera
    if len(sys.argv) > 1 and sys.argv[1] == 'bottom':
        camera_channel = CAMERA_CHANNEL.BOTTOM

    # Set camera channel for real drone
    if config['simulation'] == "False":
        from ardrone_autonomy.srv import CamSelect, CamSelectRequest
        rospy.loginfo('wait for service: {}'.format(topics['setcamchannel']['topic']))
        rospy.wait_for_service(topics['setcamchannel']['topic'])
        toggle_camera = rospy.ServiceProxy(topics['setcamchannel']['topic'], CamSelect)
        camera = CamSelectRequest()
        camera.channel = camera_channel.value

        toggle_camera(camera)

    # drone control init
    drone_control = DroneControl(config, camera_channel)

    drone_control.start()