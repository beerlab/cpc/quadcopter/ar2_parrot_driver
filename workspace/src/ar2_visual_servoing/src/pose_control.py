#!/usr/bin/env python3

# ROS
import rospy
import tf
from geometry_msgs.msg import Twist
from std_srvs.srv import Empty

# Other
import numpy as np
import sys
from enum import Enum

class PID:

    def __init__(self, kp, ki, kd, n):

        self._n = n
        self._P = 0
        self._I = 0
        self._D = 0

        self._kp = kp
        self._ki = ki
        self._kd = kd

        self._current_time = rospy.Time.now().to_sec()
        self._last_time = rospy.Time.now().to_sec()
        self._e = 0
        self._control = np.zeros((self._n,))

    def reset(self):
        self._P = 0
        self._I = 0
        self._D = 0

        self._e = 0
        self._control = np.zeros((self._n,))

    def update_pid(self, e):

        self._last_time = self._current_time
        self._current_time = rospy.Time.now().to_sec()
        self._T = self._current_time - self._last_time

        if self._T > 0.1:
            self.reset()
            return
    
        self._P = self._kp * e
        self._D = self._kd * e/self._T
        self._I += self._ki * (e + self._e) * self._T/2
        self._e = e

        self._control = self._P + self._I + self._D

    def get_control(self):
        return self._control
        

global_pid = None
global_cmd_vel_pub = None
goal_pose = (1, 1, 1)

def saturation_error(error, eps):
    error_norm = np.linalg.norm(error)
    if error_norm > eps:
        normalized_error = error / error_norm
        return normalized_error
    else:
        return error
    
def tf_callback():
        try:
            (trans, rot) = tf_listener.lookupTransform('/map_frame', '/robot_frame', rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            return

        error = goal_pose - trans[0:3] # calculte error

        error = saturation_error(error, 5)

        global_pid.update_pid(error) 
        control = global_pid.get_control()

        move_cmd = Twist()
        move_cmd.linear.x = control[0]
        move_cmd.linear.y = control[1]
        move_cmd.linear.z = control[2]

        global_cmd_vel_pub.publish(move_cmd)

        

if __name__ == '__main__':

    # Init ros
    rospy.init_node('pose_control')

    global_pid = PID(0.1, 0, 0, 3)
    global_cmd_vel_pub = rospy.Publisher('/ardrone/cmd_vel', Twist, queue_size=1)
    tf_listener = tf.TransformListener()

    rate = rospy.Rate(10.0)

    while not rospy.is_shutdown():
        tf_callback()
        rate.sleep()
