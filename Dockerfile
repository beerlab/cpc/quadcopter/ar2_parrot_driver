ARG BASE_IMG

FROM ${BASE_IMG}

SHELL ["/bin/bash", "-ci"]
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /workspace/ros_ws

RUN apt update && apt install -y \
    libspdlog-dev iputils-ping wireshark tshark iproute2 libsdl-dev libgtk2.0-dev  \ 
    ros-noetic-camera-info-manager ros-noetic-roslint \
    ros-noetic-teleop-twist-keyboard && \
    rm -rf /var/lib/apt/lists/*

RUN pip3 install --upgrade djitellopy2

COPY workspace/src/ardrone_autonomy /workspace/ros_ws/src/ardrone_autonomy

RUN catkin_make
